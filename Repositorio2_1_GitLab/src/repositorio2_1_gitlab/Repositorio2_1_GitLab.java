/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package repositorio2_1_gitlab;

import java.util.Scanner;

/**
 *
 * @author DAM105
 */
public class Repositorio2_1_GitLab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int x;
        //Creamos el proyecto
        Scanner t = new Scanner(System.in);
        System.out.println("Te enseño las opciones ");
        System.out.println("el numero 1  cambia la unidad de horas a segundos");
        System.out.println("El numero 2 cambia la unidad de kilometroas a matros");
        System.out.println("La opcion 3 cambia la conversion de km/h a m/h");
        System.out.println("Opcion 4 salir");
        do {
            System.out.println(" Introduce un numero del 1 al 4");
            x = t.nextInt();
            switch (x) {
                case 1:
                    System.out.println(" Introduce las horas");
                    int horas = t.nextInt();
                    int segundos = horas * 3600;
                    System.out.println(horas + " hora son: " + segundos + " segundos");
                    break;
                case 2:
                    System.out.println(" Introduce los kilometros ");
                    short kilometros = t.nextShort();
                    short metros = (short) (kilometros * 1000);
                    System.out.println(kilometros + " Kilometros son: " + metros + " metros.");
                    break;
                case 3:
                    System.out.println("Introduce los km/h");
                    int metross = t.nextInt();
                    double metro = metross / 3.6;
                    System.out.printf("al pasr los km/h a metros/s son: %,.2f\n ", metro, "m/s");
            }
        } while (x == 3);
        System.out.println("Has salido del programa");
    }
}
